using System.IO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

namespace crossblog.Domain
{
    public class CrossBlogContextFactory : IDesignTimeDbContextFactory<CrossBlogDbContext>
    {
        public CrossBlogDbContext CreateDbContext(string[] args)
        {
            IConfigurationRoot configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .Build();

            var builder = new DbContextOptionsBuilder<CrossBlogDbContext>();

            var connectionString = configuration.GetConnectionString("CrossBlog");

            builder.UseMySql(connectionString);       

            return new CrossBlogDbContext(builder.Options);
        }

        
    }

}