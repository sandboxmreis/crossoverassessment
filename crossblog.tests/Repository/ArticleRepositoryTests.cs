using crossblog.Domain;
using crossblog.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using Xunit;

namespace crossblog.tests.Repository
{
    public class ArticleRepositoryTests
    {
        private IArticleRepository _articleRepository;
        public ArticleRepositoryTests()
        {
            _articleRepository = GetInMemoryArticleRepository();
        }

        [Fact]
        public void New_Article()
        {
            var article = new Article()
            {
                Content = "test 1000",
                Created_At = DateTime.Now,
                Date = DateTime.Now,
                Published = true,
                Title = "teste 1000"
            };
            _articleRepository.InsertAsync(article);
            Assert.NotEqual(0, article.Id);
        }
        private IArticleRepository GetInMemoryArticleRepository()
        {
            DbContextOptions<CrossBlogDbContext> options;
            var builder = new DbContextOptionsBuilder<CrossBlogDbContext>();
            builder.UseInMemoryDatabase();
            options = builder.Options;
            CrossBlogDbContext personDataContext = new CrossBlogDbContext(options);
 
            return new ArticleRepository(personDataContext);
        }
    }
}