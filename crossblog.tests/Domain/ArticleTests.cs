using System;
using crossblog.Domain;
using Xunit;

namespace crossblog.tests.Domain
{
    public class ArticleTests
    {
        private Article article;
        public ArticleTests()
        {
            article = new Article()
            {
                Title = "Teste one",
                Content = "test test test test",
                Date = DateTime.Now,
                Published = false
            };
        }
        [Fact]
        public void New_Article_Title_Empty()
        {
            Assert.NotEmpty(article.Title);
        }
        
        [Fact]
        public void New_Article_Title_Less_255()
        {
            Assert.InRange(article.Title.Length,1,255);
        }
    }
}