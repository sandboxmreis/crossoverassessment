using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using crossblog.Controllers;
using crossblog.Domain;
using crossblog.Model;
using crossblog.Repositories;
using FizzWare.NBuilder;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Moq;
using Xunit;

namespace crossblog.tests.Controllers
{
    public class CommentControllerTests
    {
        private CommentsController _commentsController;

        private Mock<ICommentRepository> _comentsRepositoryMock = new Mock<ICommentRepository>();
        private Mock<IArticleRepository> _articleRepositoryMock = new Mock<IArticleRepository>();
        private CommentModel commentModel;
        public CommentControllerTests()
        {
            _commentsController = new CommentsController(_articleRepositoryMock.Object, _comentsRepositoryMock.Object);
            commentModel = new CommentModel()
            {
                Email = "teste@teste.com",
                Title = "test",
                Content = "test teste test",
                Date = DateTime.Now,
                Published = true
            };
        }
        [Fact]
        public async Task Get_Comments_By_Article()
        {
            _comentsRepositoryMock.Setup(m => m.GetAsync(1))
            .Returns(Task.FromResult<Comment>(Builder<Comment>.CreateNew().Build()));
            var result = await _commentsController.Get(1);

            Assert.NotNull(result);
        }
        [Fact]
        public async Task Post_New_Comment()
        {
            _comentsRepositoryMock.Setup(m => m.GetAsync(1))
           .Returns(Task.FromResult<Comment>(Builder<Comment>.CreateNew().Build()));

            _articleRepositoryMock.Setup(m => m.GetAsync(1))
                       .Returns(Task.FromResult<Article>(Builder<Article>.CreateNew().Build()));

            var result = await _commentsController.Post(1, commentModel);

            Assert.Equal(201, ((CreatedResult)result).StatusCode);

        }
        [Fact]
        public async Task Get_Comments_By_Article_And_By_Id()
        {
            _comentsRepositoryMock.Setup(m => m.GetAsync(1))
            .Returns(Task.FromResult<Comment>(Builder<Comment>.CreateNew().Build()));


            var result = await _commentsController.Get(1, 1);

            Assert.NotNull(result);
        }

        [Fact]
        public async Task Comment_Not_Found(){
              _comentsRepositoryMock.Setup(m => m.GetAsync(1))
            .Returns(Task.FromResult<Comment>(Builder<Comment>.CreateNew().Build()));
            var result = await _commentsController.Get(10);
            
            Assert.Equal(404,((NotFoundResult)result).StatusCode);
        }
    }
}