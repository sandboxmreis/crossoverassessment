using crossblog.Model;
using System;
using System.Collections.Generic;
using Xunit;

namespace crossblog.tests.Model
{
    public class CommentListModelTests
    {
        private CommentListModel commentListModel;
        public CommentListModelTests()
        {
            commentListModel = new CommentListModel();
            commentListModel.Comments = new List<CommentModel>();
            commentListModel.Comments = FillComments();
        }

        private IEnumerable<CommentModel> FillComments()
        {
            yield return new CommentModel { Content = "test", Date = DateTime.Now, Email = "test@test.com", Published = true, Title = "test" };
            yield return new CommentModel { Content = "test 1", Date = DateTime.Now, Email = "test@test.com", Published = true, Title = "test 1" };
        }
        [Fact]
        public void List_Not_Empty()
        {
            Assert.NotEmpty(commentListModel.Comments);
        }
    }
}